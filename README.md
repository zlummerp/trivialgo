A package for functions too trivial to have been put in a package before because why would we want a package with common functionality defined?

Current rules for this Repository:
1. One character variable names will not be accepted. This includes receiver names because the people that perpetuate Go standards are wrong.
2. Unintelligible variable names will not be accepted. This includes, but is not limited to, one character variable names, variable names that do not pertain to anything, and junk names with no meaning.
