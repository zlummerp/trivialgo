package trivial

// IsStringContainedInSlice is a function to search a slice for a specific term
func IsStringContainedInSlice(searchTerm string, slice []string) bool {
	for _, sliceTerm := range slice {
		if searchTerm == sliceTerm {
			return true
		}
	}
	return false
}
